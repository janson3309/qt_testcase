﻿#ifndef CUSTOMEXCEPTION_H
#define CUSTOMEXCEPTION_H

#include <QObject>
#include <QException>

class CustomException: public QException
{
public:
    CustomException(QString msg);

    QString msg() const;

private:
    QString m_msg;  // 异常消息
};

#endif // CUSTOMEXCEPTION_H
