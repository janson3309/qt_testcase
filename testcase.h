﻿#ifndef TESTCASE_H
#define TESTCASE_H

#include <QObject>
#include "customexception.h"

class TestCase : public QObject
{
    Q_OBJECT
public:
    explicit TestCase(QObject *parent = nullptr);
    Q_INVOKABLE void testfunc();

};

#endif // TESTCASE_H
