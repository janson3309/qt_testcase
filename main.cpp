﻿#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "testcase.h"

void cppRegisterToQml()
{
    qmlRegisterType<TestCase>("orz.wzzn.testcase",  1, 0, "TestCase");
}



int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    cppRegisterToQml();
    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
