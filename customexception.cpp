﻿#include "customexception.h"

CustomException::CustomException(QString msg)
    : m_msg(msg)
{

}

QString CustomException::msg() const
{
    return m_msg;
}
