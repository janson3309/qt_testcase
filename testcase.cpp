﻿#include "testcase.h"
#include <QDebug>

TestCase::TestCase(QObject *parent) : QObject(parent)
{

}

void TestCase::testfunc()
{
    try {
        throw CustomException("非法访问空指针");
    } catch(CustomException &e) {
        qDebug()<<e.msg()<<e.what();
    } catch(...) {
        qDebug()<<"未知异常";
    }

    try {
        throw -1;
    } catch(CustomException &e) {
        qDebug()<<e.msg()<<e.what();
    } catch(...) {
        qDebug()<<"未知异常";
    }
}
